use std::net::Ipv4Addr;
use std::os::unix::io::AsRawFd;
use tokio_tun::Tun;
use compio::{io::AsyncWrite, runtime::{spawn, TryClone}};

#[compio::main]
async fn main() {
    let mut tun = Tun::builder()
                .name("")
                .tap(false)
                .packet_info(false)
                .mtu(1350)
                .up()
                .address(Ipv4Addr::new(10, 0, 0, 1))
                .destination(Ipv4Addr::new(10, 1, 0, 1))
                .broadcast(Ipv4Addr::BROADCAST)
                .netmask(Ipv4Addr::new(255, 255, 255, 0))
                .try_build()
                .unwrap();

    println!("-----------");
    println!("tun created");
    println!("-----------");

    println!(
        "┌ name: {}\n├ fd: {}\n├ mtu: {}\n├ flags: {}\n├ address: {}\n├ destination: {}\n├ broadcast: {}\n└ netmask: {}",
        tun.name(),
        tun.as_raw_fd(),
        tun.mtu().unwrap(),
        tun.flags().unwrap(),
        tun.address().unwrap(),
        tun.destination().unwrap(),
        tun.broadcast().unwrap(),
        tun.netmask().unwrap(),
    );

    println!("---------------------");
    println!("ping 10.1.0.2 to test");
    println!("---------------------");

    // Writer: simply clone Tun.

    let mut tun_c = tun.try_clone().unwrap();
    spawn(async move{
        let buf = b"data to be written";
        tun_c.send_all(buf).await.unwrap();
    }).detach();

    // Reader
    let mut buf = [0u8; 1024];
    loop {
        let n = tun.recv(&mut buf).await.unwrap();
        println!("reading {} bytes: {:?}", n, &buf[..n]);
    }
}
