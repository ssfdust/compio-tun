# Compio TUN/TAP

Asynchronous allocation of TUN/TAP devices in Rust using [`compio`](https://crates.io/crates/compio). Forked from [`tokio-tun`](https://crates.io/crates/tokio-tun)

## Getting Started

- Create a tun device using `Tun::builder()` and read from it in a loop:

```rust
#[compio::main]
async fn main() {
    let tun = Tun::builder()
            .name("")            // if name is empty, then it is set by kernel.
            .tap(false)          // false (default): TUN, true: TAP.
            .packet_info(false)  // false: IFF_NO_PI, default is true.
            .up()                // or set it up manually using `sudo ip link set <tun-name> up`.
            .try_build()         // or `.try_build_mq(queues)` for multi-queue support.
            .unwrap();

    println!("tun created, name: {}, fd: {}", tun.name(), tun.as_raw_fd());

    // Writer: simply clone Tun.
    let mut tun_c = tun.try_clone().unwrap();
    spawn(async move{
        let buf = b"data to be written";
        tun_c.send_all(buf).await.unwrap();
    }).detach();

    // Reader
    let mut buf = [0u8; 1024];
    loop {
        let n = tun.recv(&mut buf).await.unwrap();
        println!("reading {} bytes: {:?}", n, &buf[..n]);
    }
}
```

- Run the code using `sudo`:

```bash
sudo -E $(which cargo) run
```

- Set the address of device (address and netmask could also be set using `TunBuilder`):

```bash
sudo ip a add 10.0.0.1/24 dev <tun-name>
```

- Ping to read packets:

```bash
ping 10.0.0.2
```

- Display devices and analyze the network traffic:

```bash
ip tuntap
sudo tshark -i <tun-name>
```

## Supported Platforms

- [x] Linux
- [ ] FreeBSD
- [ ] Android
- [ ] OSX
- [ ] iOS
- [ ] Windows

## Examples

- [`read`](examples/read.rs): Split tun to (reader, writer) pair and read packets from reader.
- [`read-mq`](examples/read-mq.rs): Read from multi-queue tun using `futures::select!`.

```bash
sudo -E $(which cargo) run --example read
```

## Thanks

- [`tokio-tun`](https://crates.io/crates/tokio-tun): Asynchronous TUN/TAP device allocation in Rust using `tokio`.
