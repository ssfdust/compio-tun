use crate::linux::interface::Interface;
use crate::linux::io::TunIo;
use crate::linux::params::Params;
use crate::Result;
use crate::TunBuilder;
use std::io::{self, ErrorKind, IoSlice};
use std::mem;
use std::net::Ipv4Addr;
use std::os::raw::c_char;
use std::os::unix::io::{AsRawFd, RawFd};
use std::sync::Arc;
use compio::io::{AsyncRead, AsyncWrite};
use compio::buf::{IoBufMut, IoBuf, IoVectoredBuf, BufResult, IntoInner};
use compio::driver::op::{BufResultExt, Recv, Send, SendVectored, Sync};
use compio::runtime::{Runtime, Attacher, TryClone};

static TUN: &[u8] = b"/dev/net/tun\0";

/// Represents a Tun/Tap device. Use [`TunBuilder`](struct.TunBuilder.html) to create a new instance of [`Tun`](struct.Tun.html).
// TODO! Implement
pub struct Tun {
    iface: Arc<Interface>,
    io: Attacher<TunIo>,
}

impl TryClone for Tun {
    fn try_clone(&self) -> io::Result<Self> {
        let io = self.io.try_clone()?;
        Ok(Self {
            iface: self.iface.clone(),
            io
        })
    }
}

impl AsRawFd for Tun {
    fn as_raw_fd(&self) -> RawFd {
        self.io.as_raw_fd()
    }
}

impl AsyncRead for Tun {
    async fn read<B: IoBufMut>(
        &mut self,
        buf: B,
    ) -> BufResult<usize, B> {
        (&*self).read(buf).await
    }
}

impl AsyncRead for &Tun {
    async fn read<B: IoBufMut>(
        &mut self,
        buf: B,
    ) -> BufResult<usize, B> {
        let fd = self.as_raw_fd();
        let op = Recv::new(fd, buf);
        Runtime::current().submit(op).await.into_inner().map_advanced()
    }
    
}

impl AsyncWrite for Tun {
    async fn write<B: IoBuf>(
        &mut self,
        buf: B,
    ) -> BufResult<usize, B> {
        (&*self).write(buf).await
    }
    
    async fn write_vectored<B: IoVectoredBuf>(
        &mut self,
        buf: B,
    ) -> BufResult<usize, B> {
        (&*self).write_vectored(buf).await
    }

    async fn flush(&mut self) -> io::Result<()> {
        (&*self).flush().await
    }

    async fn shutdown(&mut self) -> io::Result<()> {
        (&*self).shutdown().await
    }
}

impl AsyncWrite for &Tun {
    async fn write<T: IoBuf>(
        &mut self,
        buf: T,
    ) -> BufResult<usize, T> {
        let fd = self.as_raw_fd();
        let op = Send::new(fd, buf);
        Runtime::current().submit(op).await.into_inner()
    }

    async fn write_vectored<T: IoVectoredBuf>(
        &mut self,
        buf: T,
    ) -> BufResult<usize, T> {
        let fd = self.as_raw_fd();
        let op = SendVectored::new(fd, buf);
        Runtime::current().submit(op).await.into_inner()
    }

    async fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }

    async fn shutdown(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl Tun {
    pub fn builder() -> TunBuilder {
        TunBuilder::new()
    }

    /// Creates a new instance of Tun/Tap device.
    pub(crate) fn new(params: Params) -> Result<Self> {
        let iface = Self::allocate(params, 1)?;
        let fd = iface.files()[0];
        Ok(Self {
            iface: Arc::new(iface),
            io: Attacher::new(TunIo::from(fd))?,
        })
    }

    /// Creates a new instance of Tun/Tap device.
    pub(crate) fn new_mq(params: Params, queues: usize) -> Result<Vec<Self>> {
        let iface = Self::allocate(params, queues)?;
        let mut tuns = Vec::with_capacity(queues);
        let iface = Arc::new(iface);
        for &fd in iface.files() {
            tuns.push(Self {
                iface: iface.clone(),
                io: Attacher::new(TunIo::from(fd))?,
            })
        }
        Ok(tuns)
    }

    fn allocate(params: Params, queues: usize) -> Result<Interface> {
        let fds = (0..queues)
            .map(|_| unsafe {
                libc::open(
                    TUN.as_ptr().cast::<c_char>(),
                    libc::O_RDWR | libc::O_NONBLOCK,
                )
            })
            .collect::<Vec<_>>();

        let iface = Interface::new(
            fds,
            params.name.as_deref().unwrap_or_default(),
            params.flags,
        )?;
        iface.init(params)?;
        Ok(iface)
    }

    /// Receives a packet from the Tun/Tap interface.
    ///
    /// This method takes &self, so it is possible to call this method concurrently with other methods on this struct.
    pub async fn recv(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let tmp = vec![0; buf.len()];
        match self.read(tmp).await {
            BufResult(Ok(n), tmp) => {
                buf.copy_from_slice(&tmp);
                Ok(n)
            }
            BufResult(Err(e), _) => Err(e),
        }
    }

    /// Sends a buffer to the Tun/Tap interface. Returns the number of bytes written to the device.
    ///
    /// This method takes &self, so it is possible to call this method concurrently with other methods on this struct.
    pub async fn send(&mut self, buf: &[u8]) -> io::Result<usize> {
        let buf = buf.to_vec();
        match self.write(buf).await {
            BufResult(Ok(n), _) => Ok(n),
            BufResult(Err(e), _) => Err(e),
        }
    }

    /// Sends all of a buffer to the Tun/Tap interface.
    ///
    /// This method takes &self, so it is possible to call this method concurrently with other methods on this struct.
    pub async fn send_all(&mut self, buf: &[u8]) -> io::Result<()> {
        let mut remaining = buf;
        while !remaining.is_empty() {
            match self.send(remaining).await? {
                0 => return Err(ErrorKind::WriteZero.into()),
                n => {
                    let (_, rest) = mem::take(&mut remaining).split_at(n);
                    remaining = rest;
                }
            }
        }
        Ok(())
    }

    /// Sends several different buffers to the Tun/Tap interface. Returns the number of bytes written to the device.
    ///
    /// This method takes &self, so it is possible to call this method concurrently with other methods on this struct.
    pub async fn send_vectored(&mut self, bufs: &[IoSlice<'_>]) -> io::Result<usize> {
        let vec_data = bufs.iter().map(|b| b.to_vec()).collect::<Vec<_>>();
        match self.write_vectored(vec_data).await {
            BufResult(Ok(n), _) => Ok(n),
            BufResult(Err(e), _) => Err(e),
        }
    }

    /// Tries to receive a buffer from the Tun/Tap interface.
    ///
    /// When there is no pending data, `Err(io::ErrorKind::WouldBlock)` is returned.
    ///
    /// This method takes &self, so it is possible to call this method concurrently with other methods on this struct.
    pub fn try_recv(&self, buf: &mut [u8]) -> io::Result<usize> {
        self.io.recv(buf)
    }

    /// Tries to send a packet to the Tun/Tap interface.
    ///
    /// When the socket buffer is full, `Err(io::ErrorKind::WouldBlock)` is returned.
    ///
    /// This method takes &self, so it is possible to call this method concurrently with other methods on this struct.
    pub fn try_send(&self, buf: &[u8]) -> io::Result<usize> {
        self.io.send(buf)
    }

    /// Tries to send several different buffers to the Tun/Tap interface.
    ///
    /// When the socket buffer is full, `Err(io::ErrorKind::WouldBlock)` is returned.
    ///
    /// This method takes &self, so it is possible to call this method concurrently with other methods on this struct.
    pub fn try_send_vectored(&self, bufs: &[IoSlice<'_>]) -> io::Result<usize> {
        self.io.sendv(bufs)
    }

    /// Returns the name of Tun/Tap device.
    pub fn name(&self) -> &str {
        self.iface.name()
    }

    /// Returns the value of MTU.
    pub fn mtu(&self) -> Result<i32> {
        self.iface.mtu(None)
    }

    /// Returns the IPv4 address of MTU.
    pub fn address(&self) -> Result<Ipv4Addr> {
        self.iface.address(None)
    }

    /// Returns the IPv4 destination address of MTU.
    pub fn destination(&self) -> Result<Ipv4Addr> {
        self.iface.destination(None)
    }

    /// Returns the IPv4 broadcast address of MTU.
    pub fn broadcast(&self) -> Result<Ipv4Addr> {
        self.iface.broadcast(None)
    }

    /// Returns the IPv4 netmask address of MTU.
    pub fn netmask(&self) -> Result<Ipv4Addr> {
        self.iface.netmask(None)
    }

    /// Returns the flags of MTU.
    pub fn flags(&self) -> Result<i16> {
        self.iface.flags(None)
    }
}
